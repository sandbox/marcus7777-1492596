<?php
/**
 * @file
 * pop_out_and_pick.default_cer_presets.inc
 */

/**
 * Implements hook_default_cer().
 */
function pop_out_and_pick_default_cer() {
  $export = array();

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->entity_types_content_fields = 'commerce_line_item*product*field_options*option_with_help*option_with_help*field_product_ref';
  $cnr_obj->enabled = 1;
  $export['commerce_line_item*product*field_options*option_with_help*option_with_help*field_product_ref'] = $cnr_obj;

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->entity_types_content_fields = 'commerce_product*product*field_options*option_with_help*option_with_help*field_product_ref';
  $cnr_obj->enabled = 1;
  $export['commerce_product*product*field_options*option_with_help*option_with_help*field_product_ref'] = $cnr_obj;

  return $export;
}
