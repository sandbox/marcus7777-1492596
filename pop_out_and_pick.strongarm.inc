<?php
/**
 * @file
 * pop_out_and_pick.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pop_out_and_pick_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clone_method';
  $strongarm->value = 'save-edit';
  $export['clone_method'] = $strongarm;

  return $export;
}
