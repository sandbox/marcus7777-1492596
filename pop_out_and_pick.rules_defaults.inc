<?php
/**
 * @file
 * pop_out_and_pick.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function pop_out_and_pick_default_rules_configuration() {
  $items = array();
  $items['rules_option_picked'] = entity_import('rules_config', '{ "rules_option_picked" : {
      "LABEL" : "option picked",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "php", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item" ],
            "field" : "field_options_picked_er"
          }
        },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item:field-options-picked-er:0" ],
            "field" : "field_price"
          }
        }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-line-item:field-options-picked-er" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "php_eval" : { "code" : "if (($list_item-\\u003Efield_price[\\u0027und\\u0027][0][\\u0027value\\u0027] *100) \\u003E 1) {\\r\\n  $tax_rate = $commerce_line_item-\\u003Ecommerce_total[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027][1][\\u0027price\\u0027][\\u0027data\\u0027][\\u0027tax_rate\\u0027][\\u0027rate\\u0027];\\r\\n  $commerce_line_item-\\u003Ecommerce_unit_price[\\u0027und\\u0027][0][\\u0027amount\\u0027] = $commerce_line_item-\\u003Ecommerce_unit_price[\\u0027und\\u0027][0][\\u0027amount\\u0027] + ($list_item-\\u003Efield_price[\\u0027und\\u0027][0][\\u0027value\\u0027] *100);\\r\\n  $commerce_line_item-\\u003Ecommerce_total[\\u0027und\\u0027][0][\\u0027amount\\u0027] = $commerce_line_item-\\u003Ecommerce_unit_price[\\u0027und\\u0027][0][\\u0027amount\\u0027];\\r\\n\\/\\/ load it\\u0027s group\\r\\n  $option_with_help_group_loaded = entity_load(\\u0022option_with_info_group\\u0022,array($list_item-\\u003Efield_group[\\u0027und\\u0027][0][\\u0027target_id\\u0027]));\\r\\n\\r\\n  $addto_tax = ($list_item-\\u003Efield_price[\\u0027und\\u0027][0][\\u0027value\\u0027] *100) * $tax_rate;\\r\\n\\r\\n    $price_to_add = array(\\r\\n     \\u0022name\\u0022 =\\u003E $option_with_help_group_loaded[$list_item-\\u003Efield_group[\\u0027und\\u0027][0][\\u0027target_id\\u0027]]-\\u003Etitle,\\r\\n     \\u0022price\\u0022 =\\u003E array(\\r\\n      \\u0022amount\\u0022 =\\u003E ($list_item-\\u003Efield_price[\\u0027und\\u0027][0][\\u0027value\\u0027] *100) * (1 - $tax_rate),\\r\\n      \\u0022currency_code\\u0022 =\\u003E \\u0022GBP\\u0022,\\r\\n      \\u0022data\\u0022 =\\u003E array(),\\r\\n     ),\\r\\n    \\u0027included\\u0027 =\\u003E (Boolean) TRUE\\r\\n    );\\r\\n   foreach ($commerce_line_item-\\u003Ecommerce_total[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027] as $key =\\u003E $component ) {\\r\\n     if ( $component[\\u0027name\\u0027]== \\u0027tax|vat\\u0027) {\\r\\n        $commerce_line_item-\\u003Ecommerce_total[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027][$key][\\u0027price\\u0027][\\u0027amount\\u0027] = $component[\\u0027price\\u0027][\\u0027amount\\u0027] + ((Float) $addto_tax) ;\\r\\n     }\\r\\n   }\\r\\n   foreach ($commerce_line_item-\\u003Ecommerce_unit_price[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027] as $key =\\u003E $component ) {\\r\\n     if ( $component[\\u0027name\\u0027]== \\u0027tax|vat\\u0027) {\\r\\n        $commerce_line_item-\\u003Ecommerce_unit_price[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027][$key][\\u0027price\\u0027][\\u0027amount\\u0027] = $component[\\u0027price\\u0027][\\u0027amount\\u0027] + ((Float) $addto_tax) ;\\r\\n     }\\r\\n   }\\r\\n  $commerce_line_item-\\u003Ecommerce_total[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027][] = $price_to_add;\\r\\n  $commerce_line_item-\\u003Ecommerce_unit_price[\\u0027und\\u0027][0][\\u0027data\\u0027][\\u0027components\\u0027][] = $price_to_add;\\r\\n}" } }
            ]
          }
        }
      ]
    }
  }');
  return $items;
}
