<?php
/**
 * @file
 * pop_out_and_pick.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pop_out_and_pick_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "cer" && $api == "default_cer_presets") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function pop_out_and_pick_eck_bundle_info() {
  $items = array(
  'option_with_help_option_with_help' => array(
  'machine_name' => 'option_with_help_option_with_help',
  'entity_type' => 'option_with_help',
  'name' => 'option_with_help',
  'label' => 'option with help',
),
  'option_with_info_group_option_with_info_group' => array(
  'machine_name' => 'option_with_info_group_option_with_info_group',
  'entity_type' => 'option_with_info_group',
  'name' => 'option_with_info_group',
  'label' => 'Group',
),
  'option_with_info_group_rules' => array(
  'machine_name' => 'option_with_info_group_rules',
  'entity_type' => 'option_with_info_group',
  'name' => 'rules',
  'label' => 'Rules',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function pop_out_and_pick_eck_entity_type_info() {
$items = array(
       'option_with_help' => array(
  'name' => 'option_with_help',
  'label' => 'option with help',
  'properties' => array(
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
  ),
),
       'option_with_info_group' => array(
  'name' => 'option_with_info_group',
  'label' => 'Group',
  'properties' => array(
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
  ),
),
  );
  return $items;
}
