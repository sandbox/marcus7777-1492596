/*
rulesJSON = 
 '['
+ '["You can\'t combine underseat steering and an underseat rack."'
+ ',{"opt":"steering","val":345}' // underseat steering
+ ',{"opt":"luggage","val":13}'   // underseat rack
+ ']'
+',["The comfy chair interferes with underseat storage."'
+ ',{"opt":"luggage","val":13}'   // underseat rack
+ ',{"opt":"seat","val":7}'       // comfy chair 
+ ']'
+',["Gatling guns just aren\'t fun without a comfy seat, and must be mounted on open-cockpit or tiller steering."'
+ ',{"opt":"guns","val":true}' // gatling guns
+ ',[{"opt":"seat","val":7}]'     // WITHOUT comfy chair
+ ',[{"opt":"steering","val":111}'// WITHOUT open cockpit steering
+  ',{"opt":"steering","val":222}'//     OR tiller steering
// ',{"opt":"mounting","val":true}'      OR mounting bracket
+  ']'
+ ']'
+']';
*/

rulesJSON = 
 '['
 
+ '["That mirror choice is confusing."'
+ ',{"opt":"Mirror - Mirrycle both sides","val":1}' // 2 mirrors
+ ',{"opt":"Mirror - Mirrycle","val":1}'   // 1 mirror
+ ']'

+',["You\'ll need something to sit on."'
+ ',[{"opt":"BodyLink seat","val":1}'
+  ',{"opt":"BodyLink seat with brackets for smaller cyclists","val":1}'
+  ',{"opt":"Ergomesh seat","val":1}'
+  ',{"opt":"Ergomesh XL seat","val":1}'
+  ']'
+ ']'

+',["The Airflow seat cushion only fits BodyLink seats."'
+ ',{"opt":"Airflow seat cushion","val":1}'
+ ',[{"opt":"BodyLink seat","val":1}'
+  ',{"opt":"BodyLink seat with brackets for smaller cyclists","val":1}'
+  ']'
+ ']'

+',["The Microbag only fits BodyLink seats."'
+ ',{"opt":"Microbag","val":1}'
+ ',[{"opt":"BodyLink seat","val":1}'
+  ',{"opt":"BodyLink seat with brackets for smaller cyclists","val":1}'
+  ']'
+ ']'

+',["Battery lighting requires the BionX power assist system."'
+ ',{"opt":"Lighting","val":61}'
+ ',[{"opt":"Transmission","val":49}]'
+ ']'

+']';


/****************************************************************************
* Rule fires if:
*     * ALL directly included conditions are met, and
*     * if there are sets, ANY set's conditions are ALL unmet
*
* There may be logically better (neater, more complete) ways of doing this
* (eg recursively use NAND or NOR, or simply have an explicit NOT operator),
* but this is easy to explain on the rule entry form, and should give
* adequate cover.
****************************************************************************/

/****************************************************************************
* Suggested wording:
* 
* RULE NAME: [______]
* This rule states that you can't have all of these together:
*    (list of name-value pairs)
*
* Unless you have one of:
*    (list of name-value pairs)
* or one of:
*    (list of name-value pairs)
* (etc -- as many of these exception lists as desired)
****************************************************************************/


// TODO: some of the form lookup functions allow the specification of a
// hypothetical value for an opt.  If this isn't going to be used, remove it.

/////////////////////////////////////////////////////////////////////////////
// Utility functions
/////////////////////////////////////////////////////////////////////////////

String.prototype.initialLC   = function()  { return this.slice(0,1).toLowerCase() + this.slice(1); }
String.prototype.initialUC   = function()  { return this.slice(0,1).toUpperCase() + this.slice(1); }
String.prototype.filter      = function(f) { return this.split('').filter(f).join(''); }
String.prototype.depunctuate = function()  { return this.match(/[-A-Za-z0-9 ]/g).join(''); }

function all(arr,f) {
    function and(a,b) {return !!a && !!b}
    if(arguments.length==1) return arr.reduce(and,true);
    else return arr.map(f).reduce(and,true) 
}
function any(arr,f) {
    function or(a,b) {return !!a || !!b}
    if(arguments.length==1) return arr.reduce(or,false);
    else return arr.map(f).reduce(or,false) 
}

// Unique elements of an array
function nub(arr){
    var ret = [];
    for(var i in arr) if(ret.indexOf(arr[i])==-1) ret.push(arr[i]);
    return ret;
}

/////////////////////////////////////////////////////////////////////////////
// Functions to examine the form
/////////////////////////////////////////////////////////////////////////////

// Get the element corresponding to an option name
function getElem(opt) { return jQuery('[name=' + opt + ']'); }

// Is the element a checkbox?
jQuery.fn.isCheckbox = function() { return jQuery(this).is('[type=checkbox]'); }

// Return the label text attached to the option
jQuery.fn.getLabel = function() { 
    if(jQuery(this).isCheckbox()) return jQuery(this).parent().text();
    else {
        var e = jQuery(this).prev().clone();
        e.children().text('');
        return e.text().trim().initialLC().depunctuate();
    }
}
function getLabel(opt) { return getElem(opt).getLabel(); }

// What value does this option have in the form? 
jQuery.fn.formLookup = function() {
    return jQuery(this).isCheckbox()
        ? !!jQuery(this).attr('checked')
        : jQuery(this).attr('value');    
}
function formLookup(opt) { return getElem(opt).formLookup(); }

// TODO: combine with formLookup?
jQuery.fn.formSet = function(v) {
    return jQuery(this).isCheckbox()
        ? jQuery(this).attr('checked',v)
        : jQuery(this).attr('value',v); 
}
function formSet(opt,val) { return getElem(opt).formSet(val); }

// Text corresponding to a selected value
function valText(opt,/*optional*/val){
    var e = getElem(opt),
        v = arguments.length==1 ? formLookup(opt) : val;
    return e.isCheckbox()
        ? (v ? '' : 'no ') + e.getLabel()
        : e.children('[value=' + v +']').text();
}

// What values could this option take, other than the selected one? 
function otherValues(opt,/*optional*/ val) {
    var e = getElem(opt),
        v = arguments.length==1 ? formLookup(opt) : val;
    if(e.isCheckbox()) return [!v];
    else { 
        var a = new Array();
        e.children().each(function() {
            var v = jQuery(this).attr('value');
            if(v!=e.attr('value')) a.push(v);
        });
        return a.filter(function(){return this != v});
    }
}

/////////////////////////////////////////////////////////////////////////////
// Functions to analyse rules
/////////////////////////////////////////////////////////////////////////////

// Decode rules and analyze each one into an object with 3 elements:
//      .html        The message HTML associated with the rule
//      .criteria    Array of all direct criteria
//      .exceptions  Array of all exception criteria arrays
function prepareRules() {
    return jQuery.parseJSON(rulesJSON).map(function(rule){
        var ret = {html:'', criteria:[], exceptions:[]};
        for(var r in rule) {
            if(typeof rule[r] === 'object') {
                if('opt' in rule[r]) ret.criteria.push(rule[r]);
                else if (0 in rule[r]) ret.exceptions.push(rule[r]);
            } else if(typeof rule[r] === 'string') ret.html = rule[r];
        }
        return ret;    
    });
}

var rules = prepareRules();

// Returns true if rule fires {when opt set to val}. 
function testRule(rule,/* optional */ opt,val) { 
    var hypothetical = arguments.length>2;
    function t(r) { 
        if(hypothetical && opt==r.opt) return val==r.val;
        else return testLine(r);
    }
    return all(rule.criteria, t) && (
        rule.exceptions.length==0 
        || !all(rule.exceptions, function(r){ return any(r,t) })
    );
}

// test an individual rule {opt,val} pair
function testLine(r) { return formLookup(r.opt)==r.val }

// Returns rules which fire {when opt set to val}.
function testRules(/* optional */ opt,val) {
    var f = arguments.count>1
        ? function (r) {return testRule(r,opt,val)}
        : function (r) {return testRule(r)}
    return rules.filter(f);
}

// Check all rules, and display message(s) if appropriate
function applyRules(){
    var rs=testRules();
    if(rs.length==0) return;
    var d = jQuery('<div>').addClass('ruleDialog').attr('title',strings.title);
    rs.map(function(r){d.append(mkMsg(r))});
    d.dialog({modal:true
             ,resizable:false
             ,draggable:false
             ,width:jQuery(window).width()*3/4
             ,closeOnEscape:false
             ,buttons:[{text:'OK' // or use closeText and a suitable theme?
                       ,disabled:true
                       ,click:function(){ jQuery(this).dialog("close").remove(); }
                       }]
             ,close:function(){jQuery(document).updateSelects()}
             });
}

/////////////////////////////////////////////////////////////////////////////
// Functions to build the message
/////////////////////////////////////////////////////////////////////////////

var strings = {
     title:'Compatibility Note'
    ,fst1:'Before you go on, please:'
    ,fst2:'Either:'
    ,fst3:'Before you go on, please do one of the following:'
    ,or_:'or '
    ,doAll2:'both'
    ,doAll3:'do all of the following'
    ,select_:'Select '
    ,unselect_:'Unselect '
    ,selectOther_:'Select something other than '
    ,_for_:' for your '
    ,doAny2:'Either:'
    ,doAny3:'Do one of the following:'
    ,b4selector:': '
    ,comma_:', '
}

// Make the content for a rule dialog
function mkMsg(rule){
    var e = jQuery('<div>').addClass(helpBodyStyle).append(jQuery('<div>').append(rule.html));
    switch(rule.criteria.length){
    case 0: break;
    case 1: e.append(jQuery('<p>').text(strings.fst1)); break;
    case 2: e.append(jQuery('<p>').text(strings.fst2)); break;
    default: e.append(jQuery('<p>').text(strings.fst3));
    }
    e.append(jQuery('<div>').append(mkUL(rule.criteria, mkLine1)));
    
    var exceptions = rule.exceptions.filter(function(rs){ return !any(rs, testLine) });
    if(exceptions.length){
        var s = '';
        if(rule.criteria.length) s = strings.or_;
        switch(exceptions.length){
        case 1: break;
        case 2: s += strings.doAll2; break;
        default: s += strings.doAll3;
        }
        e.append(jQuery('<p>').text(s.trim().initialUC() + ':')).append(mkUL(exceptions, mkSet));
    }
    
    return e;
}

// Make a <UL> from an array. fn converts an array element into an <LI>'s innerHTML.
function mkUL(arr,fn) {
    return arr.reduce(function(e,a){return e.append(jQuery('<li>').html(fn(a)))}, jQuery('<ul>'));
}

// Make a rule dialog line, including a selector to change the relevant opt.
/*
// This variant uses 'Select something other than' wording
function mkLine1(r) { 
    var e = getElem(r.opt),
        l = e.getLabel(),
        t = e.isCheckbox()
            ? (r.val ? strings.unselect_ : strings.select_) + l
            : strings.selectOther_ + valText(r.opt,r.val) + strings._for_ + l;
    return jQuery('<span>').text(t).addClone(r.opt);
}
*/
// this variant lists all valid options.
function mkLine1(r) { return mkLine2({opt:r.opt, val:otherValues(r.opt,r.val)}) }

// Same, but with an array of vals for each opt, and instruction to select
// one of the values passed
function mkLine2(r) {
    var e = getElem(r.opt),
        l = e.getLabel(),
        t = e.isCheckbox()
            ? (r.val[0] ? strings.select_ : strings.unselect_) + l
            : strings.select_ 
                + join(r.val.map(function(v){return '<em>' + valText(r.opt,v) + '</em>'}))
                + strings._for_ + l;
    return jQuery('<span>').html(t).addClone(r.opt);
}

// Comma-concatenate a series of options, with an 'or' before the last one
function join(xs,s) {
    var last = xs.pop();
    ret = xs.join(strings.comma_);
    if(ret.length) ret += ' ' + strings.or_;
    return ret + last;
}

// Make a dialog section corresponding to the given exception set
function mkSet(exc) { 
    var r = condenseSet(exc);
    if(r.length==1) return mkLine2(r[0]);
    else return jQuery('<div>')
        .append(jQuery('<p>').text(r.length==2 ? strings.doAny2 : strings.doAny3 ))
        .append(mkUL(r,mkLine2))
}

// Take an array of {opt,val} pairs and return an array with each opt represented once only,
// with all vals associated with it in an array.
// E.g. [{a,1}, {b,1}, {b,3}] -> [{a,[1]}, {b,[1,3]}]
function condenseSet(exc){
    var ret = [], opts= [];
    for(var i in exc) {
        var o = opts.indexOf(exc[i].opt);
        if(o==-1) {
            opts.push(exc[i].opt);
            ret.push({opt:exc[i].opt,val:[exc[i].val]});
        } 
        else if(ret[o].val.indexOf(exc[i].val)==-1) ret[o].val.push(exc[i].val);
    }
    return ret;
}

// Add a selector to change the given opt
jQuery.fn.addClone = function(opt) {
    return this.append(strings.b4selector)
        .append(getElem(opt).clone().attr('name',opt + '_clone')
        .attr('disabled',false).show().formSet(formLookup(opt))
        .change(function(){ 
            getElem(opt).formSet(jQuery(this).formLookup());
            getElem(opt + '_clone').formSet(jQuery(this).formLookup());
            var b = jQuery('.ruleDialog').next().find(':button');
            if(testRules().length==0) b.attr("disabled",false).removeClass('ui-state-disabled');
            else {
                jQuery('.ruleDialog').dialog('close').remove();
                applyRules();
            }
        })
    );
}
