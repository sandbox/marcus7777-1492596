// TODO :
// add to add to cart form
// add price to header line of group.
// 
var summaryStyle = 'ui-widget ui-widget-content',
    hoverStyle = 'ui-state-hover',
    helpStyle = 'ui-widget',
    helpHeadStyle = 'ui-widget-header ui-corner-top flush',
    helpBodyStyle = 'ui-widget-content ui-corner-bottom',
    selectedStyle = 'ui-widget ui-state-default ui-corner-all ui-button-text-only';

    //but not ui-button, as it applies a spurious vertical offset in ff when applied to an <a>!
    //(even in the jQ-UI samples)
    //selectedStyle = 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'
    
jQuery.fn.isChecked = function() {
    var v = jQuery(this).attr('checked');
    if(v==true || v=='checked'||jQuery(this).attr('selected') == "selected") return true;
    else return false;
}
jQuery.fn.toggleChecked = function() {
    if(jQuery(this).isChecked()) return jQuery(this).attr('checked',false);
    else return jQuery(this).attr('checked',true);
}
jQuery.fn.andFind = function(selector) {
    if(jQuery(this).is(selector)) return jQuery(this).add(jQuery(this).find(selector));
    else return jQuery(this).find(selector);
}
jQuery.fn.firstChild = function() {
    var v = jQuery(this).children(':first-child');
    if(arguments.length) return v.filter(arguments[0]); else return v;
}
//jQuery.fn.firstParent = function(selector) { return jQuery(jQuery(this).parents(selector)[0]) } //TODO: change not checked for bugs
jQuery.fn.firstParent = function(selector) { 
    return jQuery(this).parent().closest(selector);
}
jQuery.fn.updateSelects = function() {
    var option_picked = new Array(); // TODO temp until pop out and pick is part of the 'add the cart' form
    var pickindex = 0;
    var option_prices = 0;
    jQuery('#pop-out-and-pick-form div.button a').button({ disabled: false }); // enable *all* buttons 
    jQuery('.ruleMessage').remove(); // remove *all* rule warnings 
    jQuery(this).andFind('select').each(function(){
        var oid = this.value;
        var select_optioned = this.options[this.selectedIndex].innerHTML.replace(/&amp;/g, '&'); 
        if (oid > 0 ) {
          option_picked[pickindex++] = select_optioned + ' (' + oid + ')';
          option_prices = eval(option_prices + jQuery('#pop-out-and-pick-form [data-value="'+oid+'"] .price:first').text().replace(" +£","+").replace(" -£","-")).toFixed(2);
        }
        jQuery(this).prev().find('span.select').text('');
        jQuery(this).prev().find('span.group-price').text('');
        jQuery(this).prev().html('<span class="group-label">'+jQuery(this).prev().text()+'</span><span class="select">'+select_optioned+'</span><span class="group-price">'+jQuery('#pop-out-and-pick-form [data-value="'+oid+'"] .price:first').text().replace(" +£","£")+'</span>');

        jQuery(this).parent().next().filter(':hidden').show()
            .firstChild().accordion('activate','h3[data-value=' + oid + ']')
            .parent().hide();
        jQuery(this).parent().next().firstChild().children('h3').each(function(){
            var b = jQuery(this).next().find('div.button a'); // ???
            if (jQuery(this).attr('data-value')==oid){
              b.button({ disabled: true });
            }
        });
    });
    jQuery(this).andFind('.extras').next().firstChild().each(function(){
        var t='',group_price = 0;
        jQuery(this).find('label').each(function(){
            var d = jQuery(this).firstParent('div'), // one of the checkbox
                a = d.parent().find('div.button > a > span'), // botton text span 
                s = d.prev().find('span.selected'), // ??? is this the text of what is selected 
                oid = jQuery(this).prev('input').attr('data-value');

            if(jQuery(this).prev('input').isChecked()){
                option_picked[pickindex++] = jQuery(this).prev('input').attr('rel').replace(/&amp;/g, '&') + ' (' + oid + ')';
                option_prices = eval(option_prices + jQuery('#pop-out-and-pick-form  .price[data-value="'+oid+'"]:first').text().replace(" +£","+").replace(" -£","-")).toFixed(2);
                if(t!='') t += ', ';
                t += jQuery(this).prev('input').attr('rel').replace(/&amp;/g, '&');
                a.text('Remove ' + jQuery(this).text().replace(/&amp;/g, '&')); // lets store the text in a rel
                s.addClass(selectedStyle).text('selected');
            } else {
                a.text('Choose ' + jQuery(this).text().replace(/&amp;/g, '&'));
                s.removeClass(selectedStyle).text('');
            }
        });
        if(t=='') t='none';
        jQuery(this).parent().prev().find('span.select').text(t);
        if(group_price) jQuery(this).parent().prev().find('span.group-price').text('£'+group_price);
    });
  jQuery('input#edit-line-item-fields-field-options-picked-er-und').val(option_picked.join(' ,'));
  jQuery('.field-type-commerce-price div div').text('£'+eval(jQuery('.field-type-commerce-price').attr("data-bace-price")+"+"+option_prices ).toFixed(2));
jQuery(".group-price").each(function() {  
  if (eval( jQuery(this).text().replace("£","") + 0 ) != 0) {
   text =  " £" + eval( jQuery(this).text().replace("£","") + 0).toFixed(2);
   jQuery(this).text( text );    
  }
});
  return false;
};

function choiceDivs() { return jQuery('#pop-out-and-pick-form .extras').add('#pop-out-and-pick-form .options'); } // ??? is this selecting the div to play with if so TODO make more persific 

jQuery.fn.pick = function () { // ??? what is this for 
    choiceDivs().next().filter(':not(:animated)').hide('slow');
     //TODO need to think about this scrollng to the picked item look in to adding it to show()  
     //jQuery('html, body').animate({
    //     scrollTop: jQuery(this).offset().top
   //  }, 200);
    jQuery(this).next().filter('div:hidden').show('slow'); 
}

function setColumns() {
   // Measure the width of the section labels, and move the chosen items TODO ask david why :
//    var w=0;
//    jQuery('label:has(span.select)').each(function() {
//        var s = jQuery(this).find('span.select'), 
//            t = s.text();
//        s.text('');
//        var ww = jQuery(this).width();
//        s.text(t);
//        if(ww>w) w=ww;
//    });
//    jQuery('.select').css({left: w + 50}); // TODO: check how to amend existing css clas
//    //jQuery('.popOut').css({left: w + 50, width: jQuery(window).width()-w-50}); //if absolutely positioned
jQuery(".group-price").each(function() {
  if (eval( jQuery(this).text().replace("£","") + 0 ) != 0) {
   text =  " £" + eval( jQuery(this).text().replace("£","") + 0).toFixed(2);
   jQuery(this).text( text );
  }
});      
}

jQuery(window).resize(function() { setColumns() });

jQuery(document).ready(function() {
    // massage radio sets into SELECTs
    jQuery('div:has(input[type=radio]):not(:has(div))').each(function(){ 
        var sel = jQuery('<select>').attr('name',jQuery(this).find('input[type=radio]').attr('name'));
        jQuery(this).children().each(function(){
            var r = jQuery(this).find('input[type=radio]'); // ???
            //sel.attr('name',r.attr('name'));
            var opt = jQuery('<option>').attr('value',r.attr('value')).text(jQuery(this).text());
            if(r.attr('checked')=='checked') {opt.attr('selected','selected')};
            sel.append(opt);
        });
        jQuery(this).parent().append(sel);
        jQuery(this).remove();
    });
    
    // prepare 'options' placeholder for SELECT-masking text
    jQuery('.options select').hide().prev().append(jQuery('<span>').addClass('select'));
    
    // Massage 'extras' checkbox groups to similar layout to options
    jQuery('.extras').each(function() {
        jQuery(this).removeClass('extras').before(jQuery('<div>').addClass('extras').append(
            '<label><span class="group-label">'+jQuery(this).find('legend').text()+'</span><span class="select" /><span class="group-price" /></label>'
        ));
        jQuery(this).firstChild().firstChild('legend').remove();
        jQuery(this).firstChild().firstChild().unwrap();
        jQuery(this).firstChild().find('h3 > a').append(jQuery('<span>\&nbsp;\&nbsp;</span>'))
            .append(jQuery('<span>').addClass('selected'));
        jQuery(this).firstChild().children(':has(input)').each(function() {
            jQuery(this).hide().appendTo(jQuery(this).next());
        });
    });
    
    // Add 'choose' buttons for options
    jQuery('.options').next().find('div:first-child div.option-info').each(function(){ //TODO make it just of pop out and pick items
        var oid = jQuery(this).prev().attr('data-value'), // option id 	
            p = jQuery(this).parent().parent(), // the all infomation about this group of options
            s = p.prev().find('select'); // the drop down selecter 
        jQuery(this).append(jQuery('<div class="button">')
            .append(jQuery('<a>').attr('href','#!') 
            .text('Choose ' + s.find('option[value=' + oid + ']').html().replace(/&amp;/g, '&'))
            .button()
            .click(function(){
               // TODO temp \/ s.attr('value',oid).updateSelects(); // picker the option selected
               s.attr('value',oid);
               jQuery("#pop-out-and-pick-form").updateSelects();
                // /\ end temp
               // applyRules(); //TODO add rules to UI
                p.hide('slow');
                return false;
            })
        ))
    });
    
    // Add 'choose' buttons for extras 
    // TODO move in to the check through
    // 
    jQuery('.extras ').each(function(){
        var e=jQuery(this);
        e.next().find('.option-info').each(function(){
            var i = jQuery(this).find('input');
            if(i.isChecked()){
              action = 'Choose ';
            } else {
              action = 'Remove ';
            }
            jQuery(this).append(jQuery('<div>').addClass('button')
                .append(jQuery('<a>').attr('href','#!').text(action + jQuery(this).parent().find("h3 a").attr('rel')) // TODO  add the text here 
                .button()
                .click(function(){
                    i.toggleChecked();
                   //TODO temp \/  e.updateSelects();
                    jQuery("#pop-out-and-pick-form").updateSelects();

                   // applyRules(); // TODO add rules
                    return false;
                })
            ))
        })
    });
    
    // accordionise choices, style help panels
    choiceDivs().addClass(summaryStyle)
        .click(function(){ jQuery(this).pick(); return false;})
        .next().addClass('popOut').firstChild().accordion({autoHeight: false})
        .next().addClass(helpStyle).each(function() {
            jQuery(this).firstChild().addClass(helpHeadStyle)
            .nextAll().wrapAll(jQuery('<div>').addClass(helpBodyStyle));
        });
    
    // reset everything to starting position
    choiceDivs().next().hide();
  //  jQuery('#pop-out-and-pick-form').wrap('<div style="display:none" />');
 //   jQuery( "#edit-line-item-fields-field-options-picked-er" ).wrap('<a href=#! style="background: #2698F2; color: white;font-weight:900; text-align: center; font-size: 1.5em; border-radius: 5px 5px 5px 5px; margin: 10px; display: block; padding: 10px 49px 21px;" />').parent().click(function() {
//      jQuery( "#pop-out-and-pick-form").dialog( "open" );
//    });
//    jQuery("#pop-out-and-pick-form").dialog({
//      autoOpen: false,
//     width: "80%",
//      title: "Pick your options:", //TODO move to UI settings 
//      modal: true,
//      buttons: {
//        "Pick this Configuration": function() {
//          jQuery("#pop-out-and-pick-form").dialog("close");        
//        }
//      },
//      close: function() { }
//    });
    jQuery('.field-type-commerce-price').attr("data-bace-price",jQuery('.field-type-commerce-price').text().replace(",","").replace("£",""));
    jQuery(".form-type-textfield.form-item-quantity").hide();
    jQuery('#edit-line-item-fields-field-options-picked-er input').hide();
   
    jQuery("#pop-out-and-pick-form").updateSelects();
});
